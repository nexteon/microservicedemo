package com.example.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.dao.entity.User;

public interface UserRepo extends CrudRepository<User, Long>{

}
