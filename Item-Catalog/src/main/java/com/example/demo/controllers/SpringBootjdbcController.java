package com.example.demo.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.entity.User;
import com.example.demo.repo.UserRepo;



@RestController
public class SpringBootjdbcController {
	
	
	
	//@Qualifier("myrepo")
	@Autowired
	UserRepo myrepo;
		
	
	@PostMapping("/saveuser")
	public String saveUser(@ModelAttribute User user){
		
		myrepo.save(user);
	  	return "success";
	}
	@GetMapping("getuser")
	public String saveUser(){
		
		//userrepo.save(user);
	  	return "success";
	}
	
	@GetMapping(path="/add")
	public @ResponseBody String addNewUser (@RequestParam String firstName
			, @RequestParam String lastName ) {

		User n = new User();
		n.setFirstName(firstName);
		n.setLastName(lastName);
		myrepo.save(n);
		return "Saved";
	}
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<User> getAllUsers() {
		return myrepo.findAll();
	}

	
	
	
}
